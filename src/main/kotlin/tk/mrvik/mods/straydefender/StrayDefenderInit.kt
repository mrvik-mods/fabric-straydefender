package tk.mrvik.mods.straydefender

import net.minecraft.entity.EntityCategory
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import net.minecraft.world.biome.Biome
import tk.mrvik.mods.straydefender.entities.EntityStrayDefender

const val MODID="straydefender";

fun init(){
    println("Initializing mod $MODID");
    registerItems();
    registerStrayDefenderSpawn();
}
private fun registerItems(){
    Registry.register(Registry.ITEM, Identifier(MODID, "stray_defender_egg"), EntityStrayDefender.EGG);
}

private fun registerStrayDefenderSpawn(){
    EntityStrayDefender.AllowedBiomes.forEach{
        it.getEntitySpawnList(EntityCategory.MONSTER)
            .add(Biome.SpawnEntry(EntityStrayDefender.TYPE, 20, 1, 3));
    }
}
