package tk.mrvik.mods.straydefender.client

import net.minecraft.client.render.entity.EntityRenderDispatcher
import net.minecraft.client.render.entity.StrayEntityRenderer
import net.minecraft.entity.mob.AbstractSkeletonEntity
import net.minecraft.util.Identifier
import tk.mrvik.mods.straydefender.MODID

class EntityStrayDefenderRenderer(entityRenderDispatcher: EntityRenderDispatcher) : StrayEntityRenderer(entityRenderDispatcher) {
    override fun getTexture(abstractSkeletonEntity: AbstractSkeletonEntity?): Identifier {
        return Identifier("$MODID:textures/entity/skeleton/stray_defender.png");
    }
}