package tk.mrvik.mods.straydefender.ai.goals

import net.minecraft.entity.LivingEntity
import net.minecraft.entity.ai.TargetFinder
import net.minecraft.entity.ai.TargetPredicate
import net.minecraft.entity.ai.goal.Goal
import net.minecraft.entity.ai.pathing.Path
import net.minecraft.entity.mob.MobEntityWithAi
import java.util.function.Predicate

class ConditionedFleeEntityGoal<T: LivingEntity>(
    private val mob: MobEntityWithAi,
    private val fleeClass: Class<T>,
    private val fleeDistance: Double,
    private val fleeSpeed: Double,
    private val targetPredicate: TargetPredicate
    ): Goal(){

    constructor(mob: MobEntityWithAi, fleeClass: Class<T>, fleeDistance: Double, fleeSpeed: Double, predicate: Predicate<LivingEntity>):
            this(mob, fleeClass, fleeDistance, fleeSpeed, TargetPredicate().ignoreEntityTargetRules().setPredicate(predicate));

    private var fleeEntity: LivingEntity?=null;
    private var fleePath: Path?=null;
    override fun canStart(): Boolean {
        val fent= mob.world.getClosestEntity(fleeClass, targetPredicate, mob, mob.x, mob.y, mob.z, mob.boundingBox.expand(fleeDistance, 3.0, fleeDistance))
            ?: return false;
        fleeEntity=fent;
        val targetVec=TargetFinder.findGroundTargetAwayFrom(mob, 20, 7, fent.pos)
            ?: return false;
        fleePath=mob.navigation.findPathTo(targetVec.x, targetVec.y, targetVec.z, 0);
        return fleePath!==null;
    }

    override fun start() {
        mob.navigation.startMovingAlong(fleePath, fleeSpeed);
    }

    override fun canStop(): Boolean {
        return !(fleeEntity?.isAlive?:false) || mob.distanceTo(fleeEntity)>fleeDistance;
    }

    override fun stop() {
        fleeEntity=null;
        fleePath=null;
    }
}