package tk.mrvik.mods.straydefender.ai.goals

import net.minecraft.entity.ItemEntity
import net.minecraft.entity.ai.goal.Goal
import net.minecraft.entity.ai.pathing.Path
import net.minecraft.entity.ai.pathing.PathNode
import net.minecraft.entity.mob.MobEntity
import net.minecraft.util.math.BlockPos
import tk.mrvik.mods.straydefender.util.DistanceComparator

class PickWearableItemGoal(private val mobEntity: MobEntity, private val speed: Double, private val range: Double): Goal() {
    private var targetItem: ItemEntity?=null;
    private var targetPath: Path?=null;

    override fun canStart(): Boolean {
        val lst=mobEntity.world
            .getEntities(ItemEntity::class.java, mobEntity.boundingBox.expand(range, 3.0, range)) {
                !it.cannotPickup() && mobEntity.canPickUp(it.stack);
            };
        lst.sortWith(DistanceComparator(mobEntity));
        if(lst.isEmpty()) return false;
        targetItem=lst[0];
        targetPath = mobEntity.navigation.findPathTo(targetItem, 0);
        return targetItem !==null && targetPath!==null;
    }

    override fun start() {
        mobEntity.navigation.startMovingAlong(targetPath, speed);
    }

    override fun shouldContinue(): Boolean {
        return isItemViable() && mobEntity.distanceTo(targetItem)<=1.0;
    }

    override fun canStop(): Boolean {
        return !isItemViable();
    }

    private fun isItemViable(): Boolean{
        return targetItem?.isAlive?:false && mobEntity.distanceTo(targetItem) > range;
    }
}