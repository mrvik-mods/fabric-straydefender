package tk.mrvik.mods.straydefender.util

import net.minecraft.entity.EntityType
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.effect.StatusEffect
import net.minecraft.entity.effect.StatusEffects
import net.minecraft.entity.mob.WitherSkeletonEntity
import net.minecraft.entity.mob.ZombieEntity
import net.minecraft.entity.mob.ZombieVillagerEntity
import net.minecraft.entity.passive.IronGolemEntity
import net.minecraft.entity.passive.VillagerEntity
import net.minecraft.item.*

object Utils {
    fun getEffectsFor(entity: LivingEntity): List<StatusEffect> {
        val rt = ArrayList<StatusEffect>()
        if (entity.isUndead) {
            rt.add(StatusEffects.INSTANT_HEALTH)
        } else {
            rt.add(StatusEffects.INSTANT_DAMAGE)
        }
        if (entity !is WitherSkeletonEntity) {
            rt.add(StatusEffects.WITHER)
        }
        if (entity is IronGolemEntity) {
            rt.add(StatusEffects.WEAKNESS)
            rt.add(StatusEffects.LEVITATION)
        }
        return rt
    }

    fun generateSpawnEgg(entity: EntityType<*>, color1: Int, color2: Int): Item {
        val it: Item = SpawnEggItem(entity, color1, color2, Item.Settings().group(ItemGroup.MISC));
        return it
    }

    fun isZombieVillagerConverting(entity: LivingEntity?): Boolean{
        if(entity is ZombieVillagerEntity) return isZombieVillagerConverting(entity);
        return false;
    }

    private fun isZombieVillagerConverting(zb: ZombieVillagerEntity): Boolean{
        return zb.isConverting;
    }

    fun isZombieFollowingVillager(entity: LivingEntity): Boolean{
        if(entity is ZombieEntity && entity.target!=null){
            if(VillagerEntity::class.java.isAssignableFrom(entity.target!!.javaClass)) return true;
        }
        return false;
    }

    //Compare wether item1 is better than item2
    private fun compareArmor(item1: ArmorItem, item2: ArmorItem): Boolean {
        return item1.protection>item2.protection;
    }

    fun isBetter(item1: ItemStack, item2: ItemStack): Boolean{
        val remain1=item1.maxDamage-item1.damage;
        val remain2=item2.maxDamage-item2.damage;
        if(item1.item == item2.item) {
            when {
                (item1.item is BowItem) && (item2.item is BowItem) -> return remain1 + item1.item.enchantability > remain2 + item2.item.enchantability;
                (item1.item is ArmorItem) -> return compareArmor(item1.item as ArmorItem, item2.item as ArmorItem);
            }
        }
        return remain1>remain2;
    }
}