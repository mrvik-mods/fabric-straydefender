package tk.mrvik.mods.straydefender.util

import net.minecraft.entity.Entity
import net.minecraft.entity.LivingEntity

class DistanceComparator<T: Entity>(private val mobFrom: LivingEntity): Comparator<T> {
    override fun compare(p0: T, p1: T): Int {
        return (mobFrom.distanceTo(p0)-mobFrom.distanceTo(p1)).toInt();
    }
}