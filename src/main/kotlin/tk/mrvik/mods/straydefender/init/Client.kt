package tk.mrvik.mods.straydefender.init

import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry
import tk.mrvik.mods.straydefender.client.EntityStrayDefenderRenderer
import tk.mrvik.mods.straydefender.entities.EntityStrayDefender

@Suppress("unused")
fun initClient() {
    EntityRendererRegistry.INSTANCE.register(EntityStrayDefender.TYPE) {
            entityRenderDispatcher, _ -> EntityStrayDefenderRenderer(entityRenderDispatcher)
    }
}