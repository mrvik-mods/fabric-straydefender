package tk.mrvik.mods.straydefender.entities

import net.fabricmc.fabric.api.entity.FabricEntityTypeBuilder
import net.minecraft.enchantment.Enchantments
import net.minecraft.entity.*
import net.minecraft.entity.ai.goal.*
import net.minecraft.entity.attribute.EntityAttributes
import net.minecraft.entity.damage.DamageSource
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.entity.mob.*
import net.minecraft.entity.passive.IronGolemEntity
import net.minecraft.entity.passive.WolfEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.projectile.ArrowEntity
import net.minecraft.entity.projectile.ProjectileEntity
import net.minecraft.item.ArmorItem
import net.minecraft.item.BowItem
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.nbt.CompoundTag
import net.minecraft.sound.SoundEvents
import net.minecraft.util.Hand
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.MathHelper
import net.minecraft.util.registry.Registry
import net.minecraft.world.IWorld
import net.minecraft.world.LocalDifficulty
import net.minecraft.world.World
import net.minecraft.world.biome.Biome
import tk.mrvik.mods.straydefender.MODID
import tk.mrvik.mods.straydefender.ai.goals.ConditionedFleeEntityGoal
import tk.mrvik.mods.straydefender.ai.goals.PickWearableItemGoal
import tk.mrvik.mods.straydefender.util.Utils
import java.util.function.Predicate

class EntityStrayDefender(type: EntityType<EntityStrayDefender>, worldIn: World): StrayEntity(type, worldIn) {
    companion object{
        val TYPE= Registry.register(
            Registry.ENTITY_TYPE,
            Identifier(MODID, "straydefender"),
            FabricEntityTypeBuilder.create(EntityCategory.MONSTER) {type: EntityType<EntityStrayDefender>, world: World ->
                EntityStrayDefender(type, world);
            }
                .setImmuneToFire()
                .size(EntityDimensions.fixed(1f, 2f))
                .build()
        )!!;
        val EGG=Utils.generateSpawnEgg(TYPE, 0x384050, 0x1e6582);
        val AllowedBiomes= Biome.BIOMES
            .filter{biome->biome.category!=Biome.Category.OCEAN && biome.category!=Biome.Category.RIVER};

        private val attackableClasses = mutableListOf(
            HostileEntity::class.java,
            FlyingEntity::class.java,
            ShulkerEntity::class.java,
            WolfEntity::class.java,
            SlimeEntity::class.java
        )
        private var damagedByGolem = false
        @Synchronized
        fun setDamagedByIronGolem() {
            if (!damagedByGolem) {
                damagedByGolem = true
                attackableClasses.add(IronGolemEntity::class.java)
            }
        }
    }
    private val inferredSight=true;
    private var hasBreeded=false;
    private var dontDrop=true; //Unless stated otherwise

    //Init functions
    override fun initialize(
        world: IWorld?,
        difficulty: LocalDifficulty?,
        spawnType: SpawnType?,
        entityData: EntityData?,
        entityTag: CompoundTag?
    ): EntityData? {
        val data=super.initialize(world, difficulty, spawnType, entityData, entityTag);
        setCanPickUpLoot(true);
        return data;
    }

    override fun initAttributes() {
        super.initAttributes()
        getAttributeInstance(EntityAttributes.MAX_HEALTH).baseValue=40.0;
        getAttributeInstance(EntityAttributes.FOLLOW_RANGE).baseValue=30.0;
    }

    override fun initGoals() {
        var priority=-1;
        targetSelector.add(++priority, RevengeGoal(this).setGroupRevenge())
        targetSelector.add(++priority, FollowTargetGoal(this, ZombieEntity::class.java, 0, true, true, Predicate { Utils.isZombieFollowingVillager(it) }));
        attackableClasses.forEach { cls ->
            targetSelector.add(++priority, FollowTargetGoal(this, cls, !inferredSight))
        }
        goalSelector.add(++priority, SwimGoal(this))
        goalSelector.add(++priority, FleeEntityGoal(this, WolfEntity::class.java, 6.0f, 1.4, 1.8))
        goalSelector.add(++priority, ConditionedFleeEntityGoal(this, ZombieVillagerEntity::class.java,6.0, 1.8, Predicate<LivingEntity>(Utils::isZombieVillagerConverting)))
        goalSelector.add(++priority, PickWearableItemGoal(this, 1.0, 10.0));
        goalSelector.add(++priority, MoveThroughVillageGoal(this, 1.0, true, 20){true});
        goalSelector.add(++priority, WanderAroundGoal(this, 1.0))
        goalSelector.add(++priority, LookAtEntityGoal(this, PlayerEntity::class.java, 8.0f))
        goalSelector.add(++priority, LookAtEntityGoal(this, MobEntity::class.java, 15.0f))
        goalSelector.add(++priority, LookAroundGoal(this))
    }

    override fun initEquipment(difficulty: LocalDifficulty?) {
        dontDrop=true;
        super.initEquipment(difficulty);
        EquipmentSlot.values().forEach {
            if(it.type==EquipmentSlot.Type.ARMOR){
                if(random.nextBoolean()){
                    val item= MobEntity.getEquipmentForSlot(it, random.nextInt(5));
                    equipStack(it, ItemStack(item));
                }
            }
        }
        dontDrop=false;
    }

    //Tick handling
    override fun tickMovement() {
        val headItem=getEquippedStack(EquipmentSlot.HEAD);
        val prevDamage=headItem.damage;
        super.tickMovement()
        if(headItem!=ItemStack.EMPTY) headItem.damage=prevDamage; //Reset damage for head equipment
    }

    //Attack behavior
    override fun canTarget(target: LivingEntity?): Boolean {
        if(target==null || !target.isAlive) return false;
        if(!isNear(target)) return false;
        if(Utils.isZombieVillagerConverting(target)) return false;
        if(target.attacking===this) return true;
        return canTarget(target.javaClass);
    }

    override fun canTarget(type: EntityType<*>?): Boolean {
        return type!=this.type;
    }

    private fun canTarget(cls: Class<LivingEntity>): Boolean{
        if(EndermanEntity::class.java.isAssignableFrom(cls))return false;
        return attackableClasses.stream().anyMatch{
            it.isAssignableFrom(cls);
        }
    }

    override fun attack(target: LivingEntity?, f: Float) {
        if(target==null)return;
        if(!target.isAlive){
            setTarget(null);
            return;
        }
        val itemStack=getArrowType(getStackInHand(ProjectileUtil.getHandPossiblyHolding(this, Items.BOW)));
        val projectileEntity = createArrowProjectile(target, itemStack, f);
        val d = target.x - x
        val e = target.getBodyY(0.3333333333333333) - projectileEntity.y - height*0.5;
        val g = target.z - z
        val h = MathHelper.sqrt(d * d + g * g).toDouble()
        projectileEntity.setVelocity(
            d,
            e + h * 0.20000000298023224,
            g,
            2f,
            0f
        )
        playSound(SoundEvents.ENTITY_SKELETON_SHOOT, 1.0f, 1.0f / (getRandom().nextFloat() * 0.4f + 0.8f))
        world.spawnEntity(projectileEntity)
    }

    private fun createArrowProjectile(targetEntity: LivingEntity, arrow: ItemStack?, f: Float): ProjectileEntity {
        val projectile=createArrowProjectile(arrow, f)
        if(projectile is ArrowEntity) {
            Utils.getEffectsFor(targetEntity)
                .forEach {
                    projectile.addEffect(StatusEffectInstance(it, random.nextInt(100)));
                }
        }
        return projectile;
    }

    //Handle damage
    override fun damage(source: DamageSource?, amount: Float): Boolean {
        val attacker=source?.attacker;
        if(attacker!=null && attacker is IronGolemEntity) setDamagedByIronGolem();
        return super.damage(source, amount);
    }

    //Equipment update functions
    override fun updateEnchantments(difficulty: LocalDifficulty?) {
        if(mainHandStack.item===Items.BOW){
            mainHandStack.addEnchantment(Enchantments.POWER, 5);
        }
        super.updateEnchantments(difficulty);
    }

    private fun checkAndEquip(slot: EquipmentSlot,  stack: ItemStack){
        val curItem=this.getEquippedStack(slot);
        when {
            curItem==null ->equipStack(slot, stack.copy());
            Utils.isBetter(stack, curItem) -> equipStack(slot, stack.copy());
            else ->return;
        }
        stack.count--;
    }

    override fun canPickUp(stack: ItemStack?): Boolean {
        if(stack===null||stack.isEmpty)return false;
        return Utils.isBetter(stack, getEquippedStack(MobEntity.getPreferredEquipmentSlot(stack)));
    }

    override fun equipStack(slot: EquipmentSlot?, stack: ItemStack?) {
        val oldStack=getEquippedStack(slot);
        super.equipStack(slot, stack)
        if(dontDrop || oldStack.damage>=oldStack.maxDamage) return; //Don't drop boken items. Forget about them
        oldStack.count=1;
        if(!world.isClient && oldStack!=ItemStack.EMPTY) dropStack(oldStack);
    }

    //Handle interact with players
    override fun interactMob(player: PlayerEntity, hand: Hand): Boolean {
        val stack=player.getStackInHand(hand);
        val item=stack.item;
        when {
            item==Items.BONE && !hasBreeded -> createChild(player, stack);
            item is ArmorItem -> checkAndEquip(item.slotType, stack);
            item is BowItem -> checkAndEquip(EquipmentSlot.MAINHAND, stack);
            else -> return super.interactMob(player, hand);
        }
        return true;
    }

    //Util functions
    private fun isNear(entity: LivingEntity): Boolean{
        if(entity.world!==this.world) return false;
        val maxRadio=30;
        return distanceTo(entity) <= maxRadio;
    }

    private fun createChild(ideaFrom: PlayerEntity, handStack: ItemStack){
        val ent=TYPE.spawn(world, null, null, ideaFrom,
            BlockPos(this).add(1, 0, 0),
            SpawnType.BREEDING, false, false
        )!!;
        ent.hasBreeded=true;
        handStack.count--;
        hasBreeded=true;
    }

    //Simple override of default behavior
    override fun isAngryAt(player: PlayerEntity?): Boolean {
        return target==player;
    }
}
