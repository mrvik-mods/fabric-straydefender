# Stray Defender Mod

Current version -- `1.15.1`

## Install

This mod is **not** compatible with FML (AKA Forge).
If you want to look for other mods supporting Fabric, check [curseforge](https://www.curseforge.com/minecraft/mc-mods/fabric).
Mods under fabric are installed like under forge, just copy the jar files to the mods dir under your minecraft install

This mod isn't on curseforge yet so will have to use build artifacts to install the mod.
Don't worry, it's just a zip.

* Install Fabic Loader from https://fabricmc.net/use/
* Pull mod jar from [artifacts](https://gitlab.com/mrvik-mods/fabric-straydefender/-/jobs/artifacts/master/download?job=build)
  * Use the normal jar (not de dev version as it won't boot)
* Pull in the dependencies:
  * [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api?__cf_chl_captcha_tk__=0fb28138add8064b3916429ecb79efc2041fbdfc-1576950040-0-AeNPPP_a4ufb2HuNWYMQou4pu0Od7R-yQe2KK1oA6ZDJN3Kgp6osHnThzVXoRFxVRsgCgY5ABqLPA2RVsHnrqpQ-CCvtUljLuadnV_b0G9pFePu-AyLRih67PxMkBTWV8CK1seB0rRdjZ_xKEFfIqBWrDcuiCeEdJnJRuvSAfeG3JCUxbN2CRqwdJ2OXl0m2MQdgFB1jTsgybpdDoYjZfk568T9aI3WECFXLzjD6bDs4jUyXfi4ImYiSjLs5v3vhobVJuFb6FTCW0dRxSWc-n3twPtaK_feYGB-lyB857uXAicBrTJmHrPrroyFceuo4Uva9zSIdKychY7a1FFZzk80usryLhAdgtrV3y9nBZRcEjGLU63O7SiiS30n2AQEAByZiASkMkdrlSreVk3nl92k)
  * [Fabric Language Kotlin](https://www.curseforge.com/minecraft/mc-mods/fabric-language-kotlin?__cf_chl_captcha_tk__=c9f1c4f773edd275962b9747e888ea3278772b47-1576950039-0-AZ7ajiRr-U30Is9m11yTduO4wzVFaTkCcIV1BmP48WIEHp2AlQNEWUrapvnDqU2CZCvqmUwqZiNYc_tGNNdV1DBMtKC6MdWHRK5vODzwYHuIQnA0Ee7_SqKPWLT6G57mk8JSIuL-brmZiO5NFlv7WTAIvvrRctO_Xf7seBMTQjYQDmfRevPLCHqwD0KI_oXjRaIB_fMu-u7mpGi6hLEL3ki9iwIuo7d32YlVF0z7znPtwVAb_xzdEC-2V0w6Zs6AY0ORoShfMS5gc5w1RCG5alTgj59IizvNHh_oxi6V0uEFRSpwfPdeZ839GMuyHgJ4KqZF4HPgMBa3hqDhCKcihDxTXXPzFi51wW0AsxddnJCsHOkv-Vd9JcI86j7KiaZ7Hqm--xt9sofhuZUSymcEzBZEifmWmdMt3zkDTEjQpLwW)
* Start Minecraft

## License
GPL2. See LICENSE
